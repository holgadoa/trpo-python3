import argparse
import subprocess
from itertools import count
import torch
# from tensorboard_logger import configure, log_value
import numpy as np
from models import DQNRegressor, DQNSoftmax
from trpo_agent import TRPOAgent
from utils.atari_wrapper import wrap_deepmind
import gym
import ptan
from tensorboardX import SummaryWriter
import os

def main(args):
  env = wrap_deepmind(gym.make("PongNoFrameskip-v4"))
  policy_model = DQNSoftmax(env.action_space.n)
  value_function_model = DQNRegressor()
  agent = TRPOAgent(env, policy_model, value_function_model)
  reward_array = []
  save_path = os.path.join("../saves", "trpo-" + args.name)
  os.makedirs(save_path, exist_ok=True)
  writer = SummaryWriter(comment="-trpo_" + args.name)

  for t in count():
    diagnostics = agent.step()
    for key, value in diagnostics.items():
      print("{}: {}".format(key, value))
      if(key == "Loss"):
        writer.add_scalar(key, value.mean().item(), t)
      else:
        writer.add_scalar(key, value, t)

    reward_array.append(diagnostics['Total Reward'])
    mean_reward = np.array(reward_array)[-100:].mean()
    std_reward = np.array(reward_array)[-100:].std()
    writer.add_scalar("REWARD_100", mean_reward, t)
    print('-------------------------REWARD------------------mean:', mean_reward, "std:", std_reward,"epoch:", t)
    # # log_value('score', reward, t)
    if t % 500 == 0:
      torch.save(policy_model.state_dict(), "policy_model.pth")
      torch.save(value_function_model.state_dict(), "value_model.pth")


if __name__ == "__main__":
  parser = argparse.ArgumentParser(
      formatter_class=argparse.ArgumentDefaultsHelpFormatter)
  parser.add_argument('--env', help='environment ID',
                      default='PongNoFrameskip-v4')
  parser.add_argument("-n", "--name", required=True, help="Name of the run")
  args = parser.parse_args()
  main(args)
